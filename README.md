# Docker compose
## Introduction
Here you can find docker compose configs to run [rails application](https://bitbucket.org/stanislav-arbor/test-work-api) and
[angular client](https://bitbucket.org/stanislav-arbor/test-work-client) together on you machine

## Install
First of all you should have installed docker on your machine and their deamon
mast be runned.
#### Required folder structure
├── docker folder

├── api

├── client

Rails application should be in folder named `api` and angular app should be in folder named `client`

The next step is build rails and prepare database
```
# build services
docker-compose build

# create database and run migrations to create tables
docker-compose run rails bundle exec rails db:create
docker-compose run rails bundle exec rails db:migrate

# To create some data you need to run
docker-compose run rails bundle exec rails db:seed

# If no errors we can run docker-compose
docker-compose up
```

Endpoints:

- Angular: `localhost:4200`
- Rails: `localhost:3000`
